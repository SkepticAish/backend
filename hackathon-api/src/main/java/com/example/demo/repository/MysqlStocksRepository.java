package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Stocks;


@Repository
public class MysqlStocksRepository implements StocksRepository {
	
	@Autowired
	JdbcTemplate template;
	@Override
	public List<Stocks> getAllStocks()
	{
		// TODO Auto-generated method stub
		String sql="SELECT * from Stocks";
		return template.query(sql, new StocksRowMapper()) ;
	}


	@Override
	public Stocks getStocksById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT * from Stocks WHERE id=?";
		return template.queryForObject(sql, new StocksRowMapper(), id);
	}

	@Override
	public Stocks editStocks(Stocks stock) {
		// TODO Auto-generated method stub
		String sql = "UPDATE Stocks SET buySell = ?, stockTicker = ? ,statusCode= ?,price = ?,volume=?,category=?,dtime=? WHERE id = ?";
		template.update(sql,stock.getbuySell(),stock.getstockTicker(),stock.getStatusCode(),stock.getPrice(),stock.getVolume(),stock.getCategory() ,stock.getDtime(),stock.getId());
		return stock;
	}

	@Override
	public int deleteStocks(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM Stocks WHERE id = ?";
		template.update(sql,id);
		return id;
	}

	@Override
	public Stocks addStocks(Stocks stock) {
		// TODO Auto-generated method stub
		String sql1 = "INSERT INTO Stocks(company,buySell, stockTicker,volume,price,statusCode,category,dtime) " +
				"VALUES(?,?,?,?,?,?,?,?)" ;
		template.update(sql1,stock.getCompany(),stock.getbuySell(),stock.getstockTicker(),stock.getVolume(),stock.getPrice(),stock.getStatusCode(),stock.getCategory(),stock.getDtime());
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double random=Math.random();
		
		if(random<0.5)
		{
			String sql = "UPDATE Stocks SET statusCode = 1 WHERE statusCode = 0";
			template.update(sql);
		}
		else
		{
			String sql = "UPDATE Stocks SET statusCode = 2 WHERE statusCode = 0";
			template.update(sql);
		}

		return stock;
	}

	

}


class StocksRowMapper implements RowMapper<Stocks>{
	@Override
	public Stocks mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Stocks(rs.getInt("id"),
				rs.getString("company"),
				rs.getString("buySell"),	
				rs.getString("stockTicker"), 
				rs.getDouble("price"), 
				rs.getInt("volume"), 
				rs.getInt("statuscode"),
				rs.getString("category"),
				rs.getTimestamp("dtime")
				);
	}
	
	
	
}