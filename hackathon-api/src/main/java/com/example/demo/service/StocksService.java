package com.example.demo.service;


import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Stocks;
import com.example.demo.repository.*;


@Service
public class StocksService {
	
	@Autowired
	private StocksRepository repository;
	
	public List<Stocks> getAllStocks(){
		return repository.getAllStocks();
		
	}
	
	public Stocks getStock(int id) {
		return repository.getStocksById(id);
	}

	public Stocks saveStock(Stocks stock) {
		return repository.editStocks(stock);
	}

	public Stocks newStock(Stocks stock) {
	  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	  stock.setDtime(timestamp);
		return repository.addStocks(stock);
	}

	public int deleteStock(int id) {
		return repository.deleteStocks(id);
	}


}
