package com.example.demo.controller;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.entities.Stocks;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@ActiveProfiles("h2")
@SpringBootTest
@AutoConfigureMockMvc


public class StocksControllerTest {


	@Autowired
	private MockMvc mockMvc;

	@Test
	public void createStock() throws Exception {
	   String uri = "/api/stocks/";
	   Stocks stock = new Stocks();
	   stock.setId(3);
	   stock.setbuySell("BUY");
	   stock.setstockTicker("GING");
	   
	   ObjectMapper mapper = new ObjectMapper();
   	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(stock);
	  
	   MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
	      .contentType(MediaType.APPLICATION_JSON_VALUE).content(requestJson)).andReturn();
	   
	   int status = mvcResult.getResponse().getStatus();
	   assertEquals(200, status);
	   
	  
	}
	@Test
	public void testToCheckGet() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks/")).andDo(print()).andExpect(status().isOk())
				.andReturn();

		List<Stocks> stocks = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<List<Stocks>>() {
				});

	}

	@Test
	public void testFindByIdSuccess() throws Exception {
		// 1. setup stuff

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/stocks/11")).andDo(print()).andExpect(status().isOk())
				.andReturn();

		// 3. verify the results
		Stocks stocks = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Stocks>() {
				});

		assertThat(stocks.getId()).isEqualTo(11);
	}

	
	 @Test public void testFindByIdFailure() throws Exception { 
		 // 1. setup stuff
	 
	  
	 // 2. call method under test
		 this.mockMvc.perform(get("/api/stocks/9999"))
	.andDo(print()) .andExpect(status().isInternalServerError()) .andReturn(); }
	 
}
