package com.example.demo.repository;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Stocks;

@ActiveProfiles("mysql")
@SpringBootTest

public class MySqlRepositoryTest 
{
	@Autowired
	MysqlStocksRepository mySQLStocksRepository;

	@Test
	public void testGetAllShippers()
	{
		List<Stocks> returnedList=mySQLStocksRepository.getAllStocks();
		
		assertThat(returnedList).isNotEmpty();
		
		for(Stocks stocks: returnedList)
		{
			System.out.println("Stock name: "+stocks.getstockTicker());


}
	}
}

