
/**
 * A class for mocking tests of the ShipperService
 * 
 * Note the extra comments here are for training purposes ONLY
 * 
 * In production these comments would not be included.
 */
package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import com.example.demo.entities.Stocks;
import com.example.demo.repository.StocksRepository;

@ActiveProfiles("h2")
@SpringBootTest
public class StocksServiceTest {

	// Get a "real" shipperService from the spring container
	@Autowired
	StocksService stocksService;

	// Get a "mock" or "fake" shipper repository
	// from the spring container
	@MockBean
	StocksRepository stocksRepository;
	
	@Test
	public void testGetShipper() {
		
		int testId = 5;
		String testName = "testStock";
		Stocks testStock = new Stocks();
		testStock.setId(testId);
		testStock.setstockTicker(testName);
		
		when(stocksRepository.getStocksById(testStock.getId()))
		.thenReturn(testStock);
		
		
		
		Stocks returnedStock = stocksService.getStock(testId);
		
		
		assertThat(returnedStock).isEqualTo(testStock);
	}
	
	
}
